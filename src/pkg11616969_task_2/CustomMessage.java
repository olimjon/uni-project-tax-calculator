/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_task_2;

/**
 *
 * @author Helkern
 */
public class CustomMessage {
    
    public static void Message(Object paramMessage) //Object paramMessage allows to store any type of variables.
    {
        if (paramMessage instanceof String) { //checking whether parameter to be String type
            System.out.println(String.valueOf(paramMessage)); //if true parsing it into string type
        }
        else if (paramMessage instanceof Double) { //checking whether parameter to be Double type
            System.out.println(String.valueOf(paramMessage)); // if true parsing it into string type
        }
    }
    
    public static void FinalMessage(int status, double income, double tax) //printing final message getting external params
    {
        System.out.println("-----------------------");
        System.out.println("For user with: ");
        System.out.println("Filling status: " + String.valueOf(status));
        System.out.println("Income: " + String.valueOf(income));        
        System.out.println("Tax calculated is: " + String.valueOf(tax));
        System.out.println("-----------------------");
    }
}

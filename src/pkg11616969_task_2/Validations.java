/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_task_2;

/**
 *
 * @author Helkern
 */
public class Validations {
    
    public static boolean isNumeric(String str) { // String parameter is passed into the method
        try { // try-catch prevents program halt caused by fatal errors
            double tryDouble = Double.parseDouble(str); //trying to cast the external string value into double. Failure to parse will jump to catch exception section
            if (tryDouble < 0) { //if parsed value appears to be negative isNumeric boolean method returns false
                return false;
            }
        } catch (NumberFormatException nfe) { // Errors caused falls the program to catch section and executes return false; The method eventually returns false boolean.
            return false;
        }
        return true; // If all try condition appears to be successful true boolean is returned by the method.
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_task_2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import static pkg11616969_task_2.CustomMessage.Message; //including external classes
import static pkg11616969_task_2.MixMethods.taxCalc; //including external classes
import static pkg11616969_task_2.CustomMessage.FinalMessage; //including external classes
import static pkg11616969_task_2.Validations.isNumeric; //including external classes

/**
 *
 * @author Helkern
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    
    
    
    public static void main(String[] args) {
        // TODO code application logic here
        // Creating pSts arraylist, which will play role of a kind of a variable containing multiple values inside.
        // They can be accessed by pointing string value position in array list from 0 till 3
        String pSts[] = {"Single","Married Filling Jointly or Qualified Widow(er)","Married Filling Separately","Head of Household"};
        Scanner scan = new Scanner(System.in); // Creating new instance of Scanner object
        List isValidStatus = Arrays.asList("0","1","2","3"); //Storing status lists in isValidStatus list
        
        //Status storage and validation
        String statusInput; // variable declaration
        String incomeInput; // variable declaration
        Message("Please enter your status:"); //Uses CustomMessage class which takes string parameter and executes System.out.println() function
        for(int i = 0; i < 4; i++) //Using of loop to eliminate code waste and repititions. Will repeat message 4 times specifing exact message parameters
        {
            Message("Please enter " + i + " for " + pSts[i] + ": ");          
        }
        
        statusInput = scan.next(); //reading input value and storing it in statusInput string
        while(!isNumeric(statusInput) || !isValidStatus.contains(statusInput)) //validation which prevents systen errors caused by incorrect type parsing. If whole condition result is true loop starts working. Thus checking input value to be numeric and positive value / and checking whether input value lies in the allowed range.
        {
            Message("Please enter valid positive values: 0 or 1 or 2 or 3 to select status: "); //Asking the question again
            statusInput = scan.next(); //and trying to store input value in the variable
        } // When statusInput was input correctly as expected it proceeds to next stage:
        int status = Integer.parseInt(statusInput); //storing and parsing string value of statusInput into integer value - "status"
        
        //Income storage and validation
        Message("Please enter amount of money you earn: "); //Uses CustomMessage class which takes string parameter and executes System.out.println() function
        incomeInput = scan.next(); //reading input value and storing it in statusInput string
        while(!isNumeric(incomeInput)) //checking whether input was only numeric and positive value
        {
            Message("Please enter valid positive value: "); //Asking the question again
            incomeInput = scan.next(); //and trying to store input value in the variable
        }
        double income = Double.parseDouble(incomeInput);//storing and parsing string value of statusInput into double value - "income"
        
        
        switch(status){ //using switch condition helps to break on the moment when case is true, after other conditions are ommited,
            case 0:
            {
                FinalMessage(status, income, taxCalc(income, status)); //Using taxCalc() method reduces amount of reusable code. Passing income and status variables as parameters.
                break; // If case was true, the switch condition halts after implementing previous method.
            }
            case 1:
            {
                FinalMessage(status, income, taxCalc(income, status));                
                break;
            }
            case 2:
            {
                FinalMessage(status, income, taxCalc(income, status));                
                break;
            }
            case 3:
            {
                FinalMessage(status, income, taxCalc(income, status));                
                break;
            }
        }
    }

    
}

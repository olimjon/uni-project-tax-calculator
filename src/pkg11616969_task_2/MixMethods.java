/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg11616969_task_2;

/**
 *
 * @author Helkern
 */
public class MixMethods {
    
    public static double taxCalc(double income, int status) // Method has two parameters to process inside of this method. The parameters can be derived in Main class when it is executed with specific parameters.
    {
        double tax; //declaring tax double variable.
        //db[][] - is multilevel array which stores tax level boundaries
        double db[][] = {{8350,33950,82250,171550,332950},{16700,67900,137050,208850,332950},{8350,33950,68525,104425,180475},{11950,45500,117450,190200,372950}};
        double taxRate[] = {0.1,0.15,0.25,0.28,0.33,0.35}; // Stores tax rates in double type
        if(income < db[status][0]) // Income further will be checked to determine which tax level boundaries income falls. db[status][] takes input value of status{0-3} and inner tax level boundary values
        {
            tax = income * taxRate[0]; // If condition is true, the program falls into this part and stores calculated tax derived using income input and tax rate.
            return tax; // After immediately tax value is returned to jump over the condition statement. It is important otherwise, all other options are going to be checked, this will cause tax variable to be overriden incorrectly.
        }
        else if(income < db[status][1])
        {
            tax = (income - db[status][0]) * taxRate[1] + (db[status][0]) * taxRate[0];
            return tax;
        }
        else if(income < db[status][2])
        {
            tax = (income - db[status][1]) * taxRate[2] + (db[status][1] - db[status][0]) * taxRate[1] + (db[status][0]) * taxRate[0];
            return tax;
        }
        else if(income < db[status][3])
        {
            tax = (income - db[status][2]) * taxRate[3] + (db[status][2] - db[status][1]) * taxRate[2] + (db[status][1] - db[status][0]) * taxRate[1] + (db[status][0]) * taxRate[0];
            return tax;
        }
        else if(income < db[status][4])
        {
            tax = (income - db[status][3]) * taxRate[4] + (db[status][3] - db[status][2]) * taxRate[3] + (db[status][2] - db[status][1]) * taxRate[2] + (db[status][1] - db[status][0]) * taxRate[1] + (db[status][0]) * taxRate[0];
            return tax;
        }
        else // If income appears to be none of above conditions, then it will be definetely more then highest tax level boundary of each status section.
        {
            tax = (income - db[status][4]) * taxRate[5] + (db[status][4] - db[status][3]) * taxRate[4] + (db[status][3] - db[status][2]) * taxRate[3] + (db[status][2] - db[status][1]) * taxRate[2] + (db[status][1] - db[status][0]) * taxRate[1] + (db[status][0]) * taxRate[0];
            return tax;
        }        
    }
    
}
